/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.NameTagVisibility;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

@SuppressWarnings("deprecation")
public class Scoreboards {
	
	private static Scoreboard sb1;
	public static Team red;
	public static Team blue;
	public static Team noteam;
	private static Objective kill;
	private static Objective mort;
	
	public static void setteam(){
		sb1 = Bukkit.getScoreboardManager().getNewScoreboard();
		red = sb1.registerNewTeam("Rouge");
		blue = sb1.registerNewTeam("Bleue");
		noteam = sb1.registerNewTeam("NoTeam");
		kill = sb1.registerNewObjective("kill", "playerKillCount");
		mort = sb1.registerNewObjective("mort", "deathCount");
		red.setAllowFriendlyFire(false);
		red.setNameTagVisibility(NameTagVisibility.ALWAYS);
		red.setPrefix("�c");
		blue.setAllowFriendlyFire(false);
		blue.setNameTagVisibility(NameTagVisibility.ALWAYS);
		blue.setPrefix("�9");
		kill.setDisplayName(ChatColor.GOLD+"Kills :");
		kill.setDisplaySlot(DisplaySlot.SIDEBAR);
		mort.setDisplayName(ChatColor.GOLD+"Morts :");
		mort.setDisplaySlot(DisplaySlot.SIDEBAR);
	}
	
	public static void setteamred(OfflinePlayer p){
		red.addPlayer(p);
		((Player) p).setScoreboard(sb1);
	}
	
	public static void setteamblue(OfflinePlayer p){
		blue.addPlayer(p);
		((Player) p).setScoreboard(sb1);
	}
	
	public static void setnoteam(Player p){
		noteam.addPlayer(p);
		p.setScoreboard(sb1);
	}

}
