/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Utils;

import org.bukkit.ChatColor;

public class GamePrefix {
	
	static String prefix = ChatColor.DARK_PURPLE+""+ChatColor.BOLD+"CoreRush : ";
	
	public static String getGamePrefix(){
		return prefix;
	}

}
