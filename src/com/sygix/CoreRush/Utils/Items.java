/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Items {
	
	public static void rouge(Player p){
		ItemStack item = new ItemStack(Material.WOOL, 1, (short)14);
	    ItemMeta itemm = item.getItemMeta();
	    itemm.setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Team Rouge");
	    item.setItemMeta(itemm);
	    p.getInventory().setItem(0, item);
	}
}
