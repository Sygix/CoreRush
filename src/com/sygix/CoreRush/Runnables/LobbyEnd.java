/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Runnables;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.CoreRush.CoreRush;
import com.sygix.CoreRush.Utils.BroadcastMessage;
import com.sygix.CoreRush.Utils.GamePrefix;
import com.sygix.CoreRush.Utils.GameState;
import com.sygix.CoreRush.Utils.Scoreboards;

public class LobbyEnd {
	
	public static int task;
	private static boolean run = false;
	public static int time = 30;
	
	@SuppressWarnings("deprecation")
	public static void Timer(){
		if(run == true){
			return;
		}
		run = true;
		task = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(CoreRush.getInstance(), new BukkitRunnable() {
			@Override
			public void run() {
				for ( Player pls : Bukkit.getServer().getOnlinePlayers()){
					pls.setLevel(time);
					if(time <= 10){
						if(time == 0){
							pls.playSound(pls.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1F, 1F);
							Timerstop();
						}else{
							pls.playSound(pls.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1F, 1F);
						}
						IChatBaseComponent comp = ChatSerializer.a("{\"text\": \""+ChatColor.GREEN+"Il reste "+time+" secondes !"+"\"}");
						PacketPlayOutChat paction = new PacketPlayOutChat(comp, (byte) 2);
						((CraftPlayer) pls).getHandle().playerConnection.sendPacket(paction);
					}
				}
				if(time == 30 || time == 15 || time <= 3) {
					if(time == 0) {
						Timerstop();
						GameState.setState(GameState.PREGAME);
						if((Scoreboards.blue.getSize() < Scoreboards.red.getSize()) || (Scoreboards.red.getSize() < Scoreboards.blue.getSize())){
							for(Player pls : Bukkit.getServer().getOnlinePlayers()){
								Scoreboards.setnoteam(pls);
								pls.sendMessage(GamePrefix.getGamePrefix()+ChatColor.RED+"Vous n'avez pas équilibré les teams, rééquilibrage automatique ...");
							}
						}
						for(OfflinePlayer pls : Scoreboards.noteam.getPlayers()){
							Teams.randomTeam(pls);
						}
						CoreRushRun.start();
					} else {
						BroadcastMessage.broadcast(GamePrefix.getGamePrefix()+ChatColor.RED+"La partie commence dans : "+time+" sec ! Sélectionnez votre team.");
					}
				}
				time--;
			}
		}, 20, 20);
	}
	
	public static void Timerstop() {
		if(run == true) {
			Bukkit.getServer().getScheduler().cancelTask(task);
			run = false;
		}
	}

}
