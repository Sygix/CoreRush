/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Runnables;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import com.sygix.CoreRush.CoreRush;
import com.sygix.CoreRush.Utils.GameState;

public class CoreRushRun {
	
	public static int timetask;
	private static boolean timerun = false;
	public static int time = 900;
	
	public static int stoptimetask;
	private static boolean stoptimerun = false;
	public static int stoptime = 8;
	
	@SuppressWarnings("deprecation")
	public static void start(){
		GameState.setState(GameState.GAME);
		final BossBar bb = Bukkit.createBossBar("Coeurs :", BarColor.PURPLE, BarStyle.SOLID);
		for(Player pls : Bukkit.getOnlinePlayers()){
			pls.teleport(new Location(Bukkit.getWorlds().get(0), 100.5, 72, 100.5));
			pls.setGameMode(GameMode.SURVIVAL);
			pls.getInventory().clear();
			pls.setFoodLevel(20);
			pls.setHealth(20);
			pls.setBedSpawnLocation(new Location(Bukkit.getWorlds().get(0), 100.5, 72, 100.5));
			bb.addPlayer(pls);
			for (PotionEffect effect : pls.getActivePotionEffects()){
		        pls.removePotionEffect(effect.getType());
			}
			
			if(timerun == true){
				return;
			}
			timerun = true;
			timetask = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(CoreRush.getInstance(), new BukkitRunnable() {
				@Override
				public void run() {
					long minutes = (time % 3600) / 60;
	        		long secondes = (time % 3600 ) % 60;
					if(time == 0){
					}
					time--;
				}
			}, 20, 20);
		}
	}
	
	public static void DeathMatch(){
		
	}
	
	public static void stop(){
		
	}

}
