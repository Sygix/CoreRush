/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Runnables;

import org.bukkit.OfflinePlayer;

import com.sygix.CoreRush.Utils.GameRandom;
import com.sygix.CoreRush.Utils.Scoreboards;

public class Teams {
	
	public static void randomTeam(OfflinePlayer p){
		if(Scoreboards.blue.getSize() > Scoreboards.red.getSize()){
			Scoreboards.setteamred(p);
		}else if(Scoreboards.red.getSize() > Scoreboards.blue.getSize()){
			Scoreboards.setteamred(p);
		}else if(Scoreboards.blue.getSize() == Scoreboards.red.getSize()){
			if(GameRandom.getRandom(0, 1) == 0){
				Scoreboards.setteamred(p);
			}else{
				Scoreboards.setteamblue(p);
			}
		}
	}

}
