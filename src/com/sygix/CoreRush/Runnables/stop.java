/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Runnables;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class stop {
	
	public static void Stop(){
		for(Player pls : Bukkit.getOnlinePlayers()){
			pls.kickPlayer(ChatColor.RED+"La partie est termin�e.");
		}
		for(World w : Bukkit.getWorlds()){
			File playerdata = new File(w.getWorldFolder(), "playerdata");
			File playerstats = new File(w.getWorldFolder(), "stats");
			try{
				for(File f : playerdata.listFiles()){
					f.delete();
				}
				for(File f : playerstats.listFiles()){
					f.delete();
				}
			}catch(Exception e){
			}
		}
	}

}
