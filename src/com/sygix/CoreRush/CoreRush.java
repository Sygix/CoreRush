/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.sygix.CoreRush.Events.EventManager;
import com.sygix.CoreRush.Runnables.stop;
import com.sygix.CoreRush.Utils.GameState;
import com.sygix.CoreRush.Utils.Scoreboards;


public class CoreRush extends JavaPlugin{
	
	public static CoreRush instance;
	public static ArrayList<Player> PLSnbr = new ArrayList<Player>();
	
	public static CoreRush getInstance() {
		return instance;
	}
	
	@Override
	public void onEnable(){
		getLogger().info("Chargement ...");
		instance = this;
		Scoreboards.setteam(); //Définition des teams
		EventManager.registerEvent(this);
		//getCommand("start").setExecutor(new startCommand());
		GameState.setState(GameState.LOBBY);
		getLogger().info("Chargement termine !");
	}
	
	@Override
	public void onDisable(){
		getLogger().info("Dechargement ...");
		stop.Stop();
		getLogger().info("Dechargement termine !");
	}

}
