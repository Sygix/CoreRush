/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Events;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.sygix.CoreRush.Runnables.LobbyEnd;
import com.sygix.CoreRush.Utils.GamePrefix;
import com.sygix.CoreRush.Utils.GameState;
import com.sygix.CoreRush.Utils.Scoreboards;

public class PlayerJoin implements Listener {
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		if((GameState.isState(GameState.GAME))||(GameState.isState(GameState.PREGAME))||(GameState.isState(GameState.FINISH))){
			p.kickPlayer(ChatColor.RED+"La partie est d�j� commenc�e !");
			e.setJoinMessage("");
			return;
		}
		p.teleport(new Location(Bukkit.getWorlds().get(0), 0.5, 70, 0.5));
		p.setGameMode(GameMode.ADVENTURE);
		p.setHealth(20);
		p.setFoodLevel(20);
		p.getInventory().clear();
		Scoreboards.setnoteam(p);
		int playercount = Bukkit.getServer().getOnlinePlayers().size();
		e.setJoinMessage("");
		for ( Player pls : Bukkit.getServer().getOnlinePlayers()){
			IChatBaseComponent comp = ChatSerializer.a("{\"text\": \""+GamePrefix.getGamePrefix()+ChatColor.GREEN+""+p.getName()+" a rejoint le jeu !"+" ( "+playercount+"/10 )"+"\"}");
			PacketPlayOutChat paction = new PacketPlayOutChat(comp, (byte) 2);
			((CraftPlayer) pls).getHandle().playerConnection.sendPacket(paction);
		}
		if(playercount < 2){
			p.sendMessage(GamePrefix.getGamePrefix()+ChatColor.RED+"Il faut minimum 2 joueurs pour d�marrer.");
		}
		if(playercount >= 2){
			LobbyEnd.Timer();
		}
	}

}
