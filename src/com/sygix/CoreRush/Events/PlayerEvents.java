/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Events;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.sygix.CoreRush.Runnables.Teams;
import com.sygix.CoreRush.Utils.GamePrefix;
import com.sygix.CoreRush.Utils.GameState;
import com.sygix.CoreRush.Utils.Scoreboards;

public class PlayerEvents implements Listener {
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent e){
		if(!GameState.isState(GameState.GAME)){
			e.setCancelled(true);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		Location loc = p.getLocation();
		Block b = p.getWorld().getBlockAt(loc.getBlockX(), loc.getBlockY() - 1, loc.getBlockZ());
		if(GameState.isState(GameState.LOBBY)){
			if ((b != null) && (b.getType().equals(Material.WOOL))){ //Rouge
				if(Scoreboards.red.getSize() >= 5){
					p.sendMessage(GamePrefix.getGamePrefix()+ChatColor.RED+"La team est compl�te.");
				}else{
					if(Scoreboards.blue.getPlayers().contains(p)){
						Scoreboards.blue.removePlayer(p);
					}
					Scoreboards.setteamred(p);
				}
				p.teleport(new Location(Bukkit.getWorlds().get(0), 0.5, 70, 0.5));
			}
			if ((b != null) && (b.getType().equals(Material.LAPIS_BLOCK))){ //Bleue
				if(Scoreboards.blue.getSize() >= 5){
					p.sendMessage(GamePrefix.getGamePrefix()+ChatColor.RED+"La team est compl�te.");
					
				}else{
					if(Scoreboards.red.getPlayers().contains(p)){
						Scoreboards.red.removePlayer(p);
					}
					Scoreboards.setteamblue(p);
				}
				p.teleport(new Location(Bukkit.getWorlds().get(0), 0.5, 70, 0.5));
			}
		}
	}

}
