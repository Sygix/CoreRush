/*******************************************************************************
 * Copyright (c) Sygix.
 * All rights reserved. You cannot re-distribute or modify this program.
 * http://sygix.tk/
 *******************************************************************************/ 

package com.sygix.CoreRush.Events;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.sygix.CoreRush.Utils.GamePrefix;
import com.sygix.CoreRush.Utils.GameState;

public class PlayerQuit implements Listener {
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		int playercount = Bukkit.getServer().getOnlinePlayers().size()-1;
		e.setQuitMessage("");
		for ( Player pls : Bukkit.getServer().getOnlinePlayers()){
			IChatBaseComponent comp = ChatSerializer.a("{\"text\": \""+GamePrefix.getGamePrefix()+ChatColor.GRAY+""+p.getName()+" a quitt� le jeu !"+" ( "+playercount+"/10 )"+"\"}");
			PacketPlayOutChat paction = new PacketPlayOutChat(comp, (byte) 2);
			((CraftPlayer) pls).getHandle().playerConnection.sendPacket(paction);
		}
		if(playercount < 2){
			if(GameState.isState(GameState.LOBBY)){
				//stop cooldown
			}else if(!GameState.isState(GameState.FINISH)){
				//stop partie
			}
		}
	}

}
